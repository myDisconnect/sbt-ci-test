#!/bin/bash
# shellcheck shell=bash
if [ "$(ls -A ~/targets/)" ]; then
  if [ -f ~/targets/sbt.zip ]; then
    rm -rf ~/.sbt/*
    unzip -o -qq ~/targets/sbt.zip -d ~/.sbt
    rm ~/targets/sbt.zip
  fi

  if [ -f ~/targets/ivy2.zip ]; then
    rm -rf ~/.ivy2/*
    unzip -o -qq ~/targets/ivy2.zip -d ~/.ivy2
    rm ~/targets/ivy2.zip
  fi

  if [ -f ~/targets/cache.zip ]; then
    rm -rf ~/.cache/*
    unzip -o -qq ~/targets/cache.zip -d ~/.cache
    rm ~/targets/cache.zip
  fi

  unzip -qq ~/targets/targets.zip -d ~/targets
  rm ~/targets/targets.zip

  find ~/targets -type d -name target -prune -exec sh "$BITBUCKET_CLONE_DIR/_scala-sbt/load-from-cache.sh" {} "$BITBUCKET_CLONE_DIR/" ~/targets/ \;
fi
