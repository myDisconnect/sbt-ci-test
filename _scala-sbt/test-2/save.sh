#!/bin/bash
# shellcheck shell=bash

if [ -z "$(ls -A ~/targets/)" ]; then
  # we are trying to zip everything so that last modified times are left untouched
  find "$BITBUCKET_CLONE_DIR" -type d -name target -prune -exec sh "$BITBUCKET_CLONE_DIR/_scala-sbt/save-to-cache.sh" {} "$BITBUCKET_CLONE_DIR" ~/targets \;
  cd ~/targets || exit
  zip -qq -r targets.zip .
  find . \! -name 'targets.zip' -delete
  if [ -d ~/.sbt ]; then
    cd ~/.sbt || exit
    zip -qq -r ~/targets/sbt.zip .
  fi
  if [ -d ~/.ivy2 ]; then
    cd ~/.ivy2 || exit
    zip -qq -r ~/targets/ivy2.zip .
  fi
  if [ -d ~/.cache ]; then
    cd ~/.cache || exit
    zip -qq -r ~/targets/cache.zip .
  fi
fi
