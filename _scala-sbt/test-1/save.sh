#!/bin/bash
# shellcheck shell=bash

if [ -z "$(ls -A ~/targets/)" ]; then
  find "$BITBUCKET_CLONE_DIR" -type d -name target -prune -exec sh "$BITBUCKET_CLONE_DIR/_scala-sbt/save-to-cache.sh" {} "$BITBUCKET_CLONE_DIR" ~/targets \;
fi
