#!/bin/bash
# shellcheck shell=bash

export JAVA_OPTS="-Xmx5G -Xss2M"
export SBT_OPTS="-Dsbt.io.jdktimestamps=true"