# sbt CI cache workaround

### Description

Working cache management for sbt and Bitbucket Pipelines CI.

Notice: 

This workaround uses SBT 1.0.4 because newer SBT versions (1.3.5 and lower) have issues with incremental compilation for CI.
See https://github.com/sbt/sbt/issues/4168

[Here](bitbucket-pipelines.yml) you will see 3 ways of using cache in Bitbucket pipelines:

* `scala-test` which doesn't cache `target` and re-compiles everytime
* `scala-test-custom-cache-1` which tries to cache `target` but doesn't work because of the changed timestamps in sbt.
* `scala-test-custom-cache-2` which caches all sbt related dependencies and works fine.


