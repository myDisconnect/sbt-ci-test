scalaVersion := "2.12.10"

name := "sbt-ci-test"
organization := "com.mydisconnect"
version := "1.0"

//resolvers += "Artima Maven Repository" at "http://repo.artima.com/releases"
//resolvers += Resolver.mavenLocal

libraryDependencies += "org.scalactic" %% "scalactic" % "3.1.0"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.1.0" % "test"

//publishTo in Global := Some(MavenCache("sbt-ci-test", ((baseDirectory in ThisBuild).value / "sbt-ci-test-repo")))
//publishArtifact in(Test, packageBin) := true
// Debug incremental zinc compiler
logLevel := Level.Debug
incOptions := incOptions.value.withApiDebug(true).withRelationsDebug(true)