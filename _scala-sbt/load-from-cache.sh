#!/bin/bash
# shellcheck shell=bash

path=$1
env=$2
pathToCache=$3

toMove=${path#$pathToCache}
dir=$env${toMove%/*}
#debug
#echo "from $path"

#mkdir -p "$dir"
mv "$path" "$dir"

#debug
#echo "to $dir"
