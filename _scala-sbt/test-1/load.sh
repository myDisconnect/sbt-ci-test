#!/bin/bash
# shellcheck shell=bash

if [ "$(ls -A ~/targets/)" ]; then
  find ~/targets -type d -name target -prune -exec sh "$BITBUCKET_CLONE_DIR/_scala-sbt/load-from-cache.sh" {} "$BITBUCKET_CLONE_DIR/" ~/targets/ \;
fi
