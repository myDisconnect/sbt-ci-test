package com.mydisconnect

import org.scalatest._

class MainTest extends FlatSpec with matchers.should.Matchers {

  "getHelloWorld" should """return "Hello, World!" message""" in {
    Main.getHelloWorld should be
    "Hello, World!"
  }

}
